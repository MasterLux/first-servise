<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_model extends CI_Model {

	function __construct()
	{
		parent::__construct();//mysqli_connect($hostname,$username,$password,$database,$port,$socket);
	}
	/**
	 *
	 */
	function myNewConnect($query)
	{
		$this->config->load('best/db');
		$dbHost = $this->config->item('dbHost');
		$dbName = $this->config->item('dbName');
		$dbUser = $this->config->item('dbUser');
		$dbPass = $this->config->item('dbPass');
		//
		$e = null;
		$href = @mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
		if (! $href)
		{
			echo "Ошибка: Невозможно установить соединение с MySQL.".PHP_EOL;
			echo "<br>Код ошибки errno: ".mysqli_connect_errno().PHP_EOL;
			echo "<br>Текст ошибки error: ".mysqli_connect_error().PHP_EOL;
			die('<br>Нет подключения.');
		}
		//
		mysqli_set_charset($href, "utf8");
		if (empty($query)) return;
		$x = trim($query);
		$e = mysqli_query($href, $x);
		mysqli_close($href);
		return $e;
	}
	/**
	 *
	 */
}
?>