<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class First extends CI_Model {

	public $my_public_key;
	public $my_signature;
	public $my_data;

	function __construct()
	{
		parent::__construct();
		define('SERVICE_KEY', 'dBQIGFGjUqlpJ9Po');
		define('BASE_URL', 'http://'.$_SERVER['SERVER_NAME'].'/');
		define('TABLE_001', 'table_001');
	}
	/**
	 *
	 */
	function home()
	{
		$str = '';
		$in_total = rand(1, 10);
		for ($i = 0; $i < $in_total; $i++)
		{
			// идентификатор транзакции
			$number = mt_rand(111111111, 999999999);
			// сумма
			$sum = mt_rand(10, 500);
			// случайная коммиссия (от 0,5% до 2%)
			$rand_commision = range(0.5, 2, 0.1);
			$index = mt_rand(0, count($rand_commision) - 1);
			$commision = $rand_commision[$index];
			// идентификатор клиента
			$order_number = mt_rand(1, 20);
			$str .= "('".$number."','".$sum."','".$commision."','".$order_number."'),";
		}
		$val = trim($str, ',');
		$this->load->model('Db_model');
		$col = '(number, sum, commision, order_number)';
		$e = "INSERT INTO ".TABLE_001." ".$col." VALUES ".$val;
		$this->Db_model->myNewConnect($e);
		// создаём подпись
		$this->my_key();
		// отправляем в работу
		$this->go_step_by_step();
	}
	/**
	 * Отправка запроса для цикличной обработки
	 */
	function go_step_by_step()
	{
		$data = array('key'=>SERVICE_KEY);
		$url = BASE_URL.'welcome/complete_step';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_exec($ch);
		curl_close($ch);
	}
	/**
	 * Обработка очереди для отправки
	 */
	function step_by_step($key)
	{
		if ($key == SERVICE_KEY)
		{
			// создаём подпись
			$this->my_key();
			//
			$this->load->model('Db_model');
			$e = "SELECT * FROM ".TABLE_001." WHERE st_1='0' LIMIT 1";
			$result = $this->Db_model->myNewConnect($e);
			if (mysqli_num_rows($result) > 0)
			{
				$e = mysqli_fetch_assoc($result);
				//
				$data = array(
					'action'=>'true', 'public_key'=>$this->my_public_key, 'signature'=>$this->my_signature, 'str'=>$this->my_data,
					'number'=>$e['number'], 'sum'=>$e['sum'], 'commision'=>$e['commision'], 'order_number'=>$e['order_number']
				);
				$this->go_send($data);
				//
				$number = $e['number'];
				$pubDate = date('Y-m-d H:i:s');
				$e = "UPDATE ".TABLE_001." SET st_1='1', pubDate='".$pubDate."' WHERE number='".$number."'";
				$result = $this->Db_model->myNewConnect($e);
				// Через 20 секунд повтор
				sleep(20);
				$this->go_step_by_step();
			}
		}
	}
	/**
	 * Отправка на второй сервис
	 */
	function go_send($data)
	{
		$url = 'http://a2.set/';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_exec($ch);
		curl_close($ch);
	}
	/**
	 *
	 */
	function my_key()
	{
		$data = 'my-key-issued-by-the-service';
		$new_key_pair = openssl_pkey_new(array("private_key_bits" => 2048,"private_key_type" => OPENSSL_KEYTYPE_RSA));
		openssl_pkey_export($new_key_pair, $private_key_pem);
		$details = openssl_pkey_get_details($new_key_pair);
		$public_key_pem = $details['key'];
		openssl_sign($data, $signature, $private_key_pem, OPENSSL_ALGO_SHA256);
		//
		$this->my_public_key = $public_key_pem;
		$this->my_signature = $signature;
		$this->my_data = $data;
		//
		//file_put_contents('private_key.pem', $private_key_pem);
		//file_put_contents('public_key.pem', $public_key_pem);
		//file_put_contents('signature.dat', $signature);
	}
}
