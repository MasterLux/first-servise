<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->model('First'); $this->First->home();
	}
	/**
	 *
	 */
	function complete_step()
	{
		if (isset($_POST['key'])) $key = $_POST['key']; else return;
		$this->load->model('First'); $this->First->step_by_step($key);
	}
}
