-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Авг 30 2020 г., 17:37
-- Версия сервера: 5.7.17-log
-- Версия PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `service_money`
--

-- --------------------------------------------------------

--
-- Структура таблицы `table_001`
--

CREATE TABLE `table_001` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `sum` float NOT NULL DEFAULT '0',
  `commision` float NOT NULL DEFAULT '0',
  `order_number` int(11) NOT NULL DEFAULT '0',
  `st_1` int(1) NOT NULL DEFAULT '0',
  `pubDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `table_001`
--

INSERT INTO `table_001` (`id`, `number`, `sum`, `commision`, `order_number`, `st_1`, `pubDate`) VALUES
(1, 154603584, 348, 0.7, 8, 1, '2020-08-30 07:05:58'),
(2, 591701514, 221, 1.5, 15, 1, '2020-08-30 07:06:08'),
(3, 347093595, 229, 2, 10, 1, '2020-08-30 07:06:19'),
(4, 633739869, 464, 1.9, 7, 1, '2020-08-30 07:06:29'),
(5, 204709370, 494, 1.8, 14, 1, '2020-08-30 07:06:39'),
(6, 695670332, 385, 1.9, 5, 1, '2020-08-30 07:06:49'),
(7, 507256782, 80, 1.3, 1, 1, '2020-08-30 07:06:59'),
(8, 633297423, 456, 1.7, 19, 1, '2020-08-30 07:07:09'),
(9, 417652916, 396, 1.3, 19, 1, '2020-08-30 07:07:19'),
(10, 423970281, 191, 1.8, 9, 1, '2020-08-30 07:07:29'),
(11, 508577684, 482, 1.1, 15, 1, '2020-08-30 08:32:35'),
(12, 137567218, 50, 1.8, 2, 1, '2020-08-30 08:32:46'),
(13, 579865010, 23, 1.4, 6, 1, '2020-08-30 08:32:56'),
(14, 158942221, 151, 1.6, 13, 1, '2020-08-30 08:33:06'),
(15, 978947813, 426, 1.7, 19, 1, '2020-08-30 08:33:16'),
(16, 318094138, 425, 0.8, 18, 1, '2020-08-30 08:33:26'),
(17, 251035752, 97, 0.8, 5, 1, '2020-08-30 08:33:36'),
(18, 209112287, 201, 1.6, 4, 1, '2020-08-30 08:33:46'),
(19, 782925374, 386, 1.8, 3, 1, '2020-08-30 08:33:57'),
(20, 933192847, 288, 1.2, 11, 1, '2020-08-30 08:34:07');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `table_001`
--
ALTER TABLE `table_001`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `table_001`
--
ALTER TABLE `table_001`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
